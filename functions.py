import string
import time

#Carl D & Emil
def func_Hypotenusa(bas, hojd):
    hypotenusa = 0
    #Gör en funktion som räknar ut hypotenusan när 
    #man får reda på basen och höjden av en rätvinklig triangel
    return hypotenusa
  
#Ymer & Mikael  
def func_rArea(bas, hojd):
    area = 0
    #Gör en funktion som räknar ut area av en rektangel med värdena som fås av användaren.
    #Returnera arean
    return area

#Carl D & Emil
def func_Medelvarde(lista_med_varden):
    medel = 0
    #Du får en lista av användaren med massa siffror.
    #Gör en funktion som räknar ut medelvärdet av siffrorna i denna lista.
    return medel

#Linus & Jesper
def func_isPalindrom(mening):
    #Ett palindrom är en sträng/mening/ord som läses likadant baklänges som framlänges se exemplen nedan.
    #Ex: "naturrutan", "kajak", "tillit", "mus rev inuits öra, sa röst i universum"
    #Man ignorerar whitespaces och komman (",") etc, bara bokstäverna betyder något.
    mening = mening.replace(',','')
    for s in string.whitespace:
        mening = mening.replace(s, '')
    print (mening)
    return mening == mening[::-1]

#Linus & Jesper 
def func_Delay(sekunder):
    #Skapa en funktion som ger en fördröjning på så många sekunder som användaren anger.
    #Vid varje sekunds fördröjning ska funktionen skriva ut en punkt (ex: .... <- efter 4 sec)
	for x in range(0, sekunder):
		time.sleep(1.0)
		print(".", end="")
	#return

#Markus M & William
def func_Sortera(sting):
	##sorterad_string = join([sorted_string].sort(key = letters))
    #Skapa en funktion som sorterar en sträng som kommer in i bokstavsordning.
    #Strängen som kommer in kommer innehålla flera ord, och det är de som ska sorteras i bokstavsordning.
    #Orden i strängen som kommer in kommer vara separerade med ett bindestreck, se exempel nedan.
    #Ex: (trollfabrik-tomater-fisk-påfågel) ska komma tillbaka som (fisk-påfågel-tomater-trollfabrik)
    return ''.join(sorted(sting))

#Jonathan & Carl S
def func_isPrime(nummer):
    test = False
	#Gör en funtion som kollar om en siffra är ett primtal. Om det är ett primtal skicka tillbaka "True", annars "False".
    return test

#Jonathan & Carl S
def func_hittaPrimtal(nummer):
    primes = []
    #Gör en funktion som hittar alla primtal upp till numret som skickats in till funktionen.
    #Spara primtalen i en lista.
    #Returnera detta som en lista.
    #Använd gärna funktionen som vi skapat för att kolla om ett tal är primtal (func_isPrime())
    return primes

#William & Markus M
def func_perfectNum(nummer):
    is_perfect = False
    #Gör en funktion som provar om ett tal är "perfekt". Vad är ett "perfekt" tal då? Kolla nedan.
    #Ett perfekt tal är ett tal där alla dess delbara heltal summerat är lika med sig själv.
    #Ex: Det första perfekta talet är 6 eftersom 6 är delbart med 1, 2 och 3. Summan av 1 + 2 + 3 = 6.
    #Ledtråd: Använd modulo (%).
    return is_perfect

#Ymer & Mikael
def func_Fakultet(nummer):
    svar = 0
    #Skapa en funktion som räknar ut "fakulteten" av talet som skickas in.
    #Ett tal i fakultet skrivs "3!" och betyder att man multiplicerar talet med alla heltal som är lägre än sig själv.
    #I ovanstående exempel skulle det bli 3 * 2 * 1 = 6
    #Skickar användaren in en 6:a ska funtionen alltså ta 6*5*4*3*2*1 = 720
    #Returnera svaret
    return svar

#Jonas & Andreas A
def func_ceasarCrypt(string, nummer):
    krypt = ""
    #Skapa en funktion som krypterar en sträng med hjälp av ceasarkryptering.
    #Ceasar-kryptering fungerar på så sätt att man tar en bokstav i taget och flyttar fram den ett visst antal bokstäver framåt.
    #Om jag vill göra en kryptering på en sträng behöver jag först välja antalet tecken jag vill flytta i strängen. I mitt exempel väljer jag 10 steg.
    #"Tobias äger" börjar med att jag kollar 10 bokstäver fram på stora T i en ASCII-tabell vilket blir tecknet ^. Sen fortsätter jag bokstav för bokstav.
    #Färdig kryptering av strängen "Tobias äger": ^ylsk}*Äqo|
    #Ovanstående är gjort för hand men är förhoppningsvis rätt. Varje bokstav 10 steg framåt i ASCII-tabellen.
    #Returnera krypterad text
    return krypt

#Jonas & Andreas A
def func_ceasarDecrypt(string, nummer):
    decrypt = ""
    #Skapa en motsatt funktion som ovan, men som istället för att gå "nummer" steg framåt i ASCII-tabellen, gå istället bakåt.
    return decrypt

print("Välkommen till multiprogrammet som gör det mesta. Välj vilket delprogram du vill köra")
print("1. Räkna ut hypotenusa")
print("2. Räkna ut en rektangels area")
print("3. Räkna ut medelvärdet av en lista")
print("4. Är en sträng/mening ett palindrom?")
print("5. Skapa delay på x antal sekunder")
print("6. Sortera några ord i bokstavsordning")
print("7. Kolla om ett tal är ett primtal")
print("8. Kolla vilka primtal som finns upp till ett visst tal")
print("9. Testa om ett nummer är 'PERFEKT'")
print("10. Vad blir ett tal i fakultet?")
print("11. Kryptera en text med ceasarkryptering")
print("12. Dekryptera en text som använder ceasarkryptering")
svar = 0
val = int(input("Skriv in ditt val: "))
match val:
	case 1:
		print("Du vill räkna ut hypotenusan på en triangel. Skriv in bas och höjd för att få reda på hypotenusan.")
		bas = int(input("Skriv in bas: "))
		hojd = int(input("Skriv in höjd: "))
		svar = func_Hypotenusa(bas, hojd)
	case 2:
		print("Vill du veta arean av en rektangel? Fyll i hur långa sidorna i nedan och låt funktionen göra jobbet")
		sida1 = int(input("Skriv in första sidans längd: "))
		sida2 = int(input("Skriv in andra sidans längd: "))
		svar = func_rArea(sida1, sida2)
	case 3:
		lista = []
		print("Du söker medelvärdet av en massa värden. Följ instruktionerna i programmet och låt funktionen göra sin magic.")
		antalnr = int(input("Skriv in hur många nummer du vill använda att få ut medelvärde för: "))
		for x in range(antalnr):
			lista.append(int(input(f"Skriv in nr:{x+1}:s värde: ")))
		svar = func_Medelvarde(lista)
	case 4:
		print("Är en mening eller ett ord ett palindrom? Testa och skriv ett ord och se resultatet!")
		pal_test = input("Skriv ordet/meningen du vill testa om det är ett palindrom: ")
		if func_isPalindrom(pal_test):
			print(f"{pal_test} IS A FRICKIN' PALINDROME, MAN!")
		else:
			print(f"No luck.. {pal_test} is no palindrom")
	case 5:
		print("Så det finns en funktion som skapar delay? Ja, om nån har programmerat den! prova och skriv nåt å se om det blir delay.")
		ord = input("Skriv in det ord du vill ska skrivas ut med delay: ")
		sek = int(input("Skriv in hur många sekunder du vill att delayen ska vara: "))
		func_Delay(sek)
		print(ord)
	case 6:
		print("Sortera en sträng i bokstavsordning. Skriv '-' (ett bindestreck) mellan orden du vill sortera")
		print("Exempel: hej-yggdrasil-tomte-bonde")
		ord = input("Skriv in det du vill sortera i bokstavsordning: ")
		sorterat_ord = func_Sortera(ord)
		print(f"Först skrev du in: {ord}")
		print("Det blev till:")
		print(sorterat_ord)
	case 7:
		print("Du vill prova om ett tal är primtal? Prova på!")
		tal = int(input("Skriv in ett tal och se om det är ett primtal: "))
		print(f"{tal} är ett primtal?")
		print(func_isPrime(tal))
	case 8:
		print("Dags att prova att få en lista med massa jäkla primtal upp till ett visst värde.")
		varde = int(input("Skriv in hur högt värde du vill hitta alla primtal upp till: "))
		all_primes = func_hittaPrimtal(varde)
		print("Denna lista som skrivs ut nedan är alla primtal upp till det tal du skrev in: ")
		print(all_primes)
	case 9:
		print("Testa om ett nummer är perfekt!")
		tal = int(input("Skriv in det tal du vill testa: "))
		print(func_perfectNum(tal))
	case 10:
		tal = int(input("Skriv in det tal du vill prova i fakultet: "))
		svar = func_Fakultet(tal)
	case 11:
		chiffer = int(input("Kryptera en text! Skriv in chiffer (ett heltal mellan 1-20): "))
		text = input("Skriv in texten du vill kryptera: ")
		kryptad = func_ceasarCrypt(text, chiffer)
		print("Din text krypterad: ")
		print(kryptad)
	case 12:
		print("Decrypt dax. Du behöver dels den krypterade strängen och dess chiffer")
		chiffer = int(input("Skriv in det chiffer som användes vid kryptering (ett heltal mellan 1-20): "))
		text = input("Skriv in texten du vill dekryptera: ")
		kryptad = func_ceasarDecrypt(text, chiffer)
	case other:
		print("Whatever")
